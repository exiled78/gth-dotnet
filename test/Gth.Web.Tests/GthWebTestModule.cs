using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Gth.Web.Startup;
namespace Gth.Web.Tests
{
    [DependsOn(
        typeof(GthWebModule),
        typeof(AbpAspNetCoreTestBaseModule)
        )]
    public class GthWebTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(GthWebTestModule).GetAssembly());
        }
    }
}