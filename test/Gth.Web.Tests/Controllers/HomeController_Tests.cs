﻿using System.Threading.Tasks;
using Gth.Web.Controllers;
using Shouldly;
using Xunit;

namespace Gth.Web.Tests.Controllers
{
    public class HomeController_Tests: GthWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}
