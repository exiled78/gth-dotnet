using Gth.EntityFrameworkCore;

namespace Gth.Tests.TestDatas
{
    public class TestDataBuilder
    {
        private readonly GthDbContext _context;

        public TestDataBuilder(GthDbContext context)
        {
            _context = context;
        }

        public void Build()
        {
            //create test data here...
        }
    }
}