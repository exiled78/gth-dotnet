﻿using System;
using System.Threading.Tasks;
using Abp.TestBase;
using Gth.EntityFrameworkCore;
using Gth.Tests.TestDatas;

namespace Gth.Tests
{
    public class GthTestBase : AbpIntegratedTestBase<GthTestModule>
    {
        public GthTestBase()
        {
            UsingDbContext(context => new TestDataBuilder(context).Build());
        }

        protected virtual void UsingDbContext(Action<GthDbContext> action)
        {
            using (var context = LocalIocManager.Resolve<GthDbContext>())
            {
                action(context);
                context.SaveChanges();
            }
        }

        protected virtual T UsingDbContext<T>(Func<GthDbContext, T> func)
        {
            T result;

            using (var context = LocalIocManager.Resolve<GthDbContext>())
            {
                result = func(context);
                context.SaveChanges();
            }

            return result;
        }

        protected virtual async Task UsingDbContextAsync(Func<GthDbContext, Task> action)
        {
            using (var context = LocalIocManager.Resolve<GthDbContext>())
            {
                await action(context);
                await context.SaveChangesAsync(true);
            }
        }

        protected virtual async Task<T> UsingDbContextAsync<T>(Func<GthDbContext, Task<T>> func)
        {
            T result;

            using (var context = LocalIocManager.Resolve<GthDbContext>())
            {
                result = await func(context);
                context.SaveChanges();
            }

            return result;
        }
    }
}
