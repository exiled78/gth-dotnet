﻿using Microsoft.EntityFrameworkCore;

namespace Gth.EntityFrameworkCore
{
    public static class DbContextOptionsConfigurer
    {
        public static void Configure(
            DbContextOptionsBuilder<GthDbContext> dbContextOptions, 
            string connectionString
            )
        {
            /* This is the single point to configure DbContextOptions for GthDbContext */
            dbContextOptions.UseSqlServer(connectionString);
        }
    }
}
