﻿using Gth.Configuration;
using Gth.Web;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Gth.EntityFrameworkCore
{
    /* This class is needed to run EF Core PMC commands. Not used anywhere else */
    public class GthDbContextFactory : IDesignTimeDbContextFactory<GthDbContext>
    {
        public GthDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<GthDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            DbContextOptionsConfigurer.Configure(
                builder,
                configuration.GetConnectionString(GthConsts.ConnectionStringName)
            );

            return new GthDbContext(builder.Options);
        }
    }
}