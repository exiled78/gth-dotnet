﻿using Abp.EntityFrameworkCore;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Gth.EntityFrameworkCore
{
    [DependsOn(
        typeof(GthCoreModule), 
        typeof(AbpEntityFrameworkCoreModule))]
    public class GthEntityFrameworkCoreModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(GthEntityFrameworkCoreModule).GetAssembly());
        }
    }
}