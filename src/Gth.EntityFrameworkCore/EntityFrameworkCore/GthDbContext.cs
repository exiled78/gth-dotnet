﻿using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Gth.EntityFrameworkCore
{
    public class GthDbContext : AbpDbContext
    {
        //Add DbSet properties for your entities...

        public GthDbContext(DbContextOptions<GthDbContext> options) 
            : base(options)
        {

        }
    }
}
