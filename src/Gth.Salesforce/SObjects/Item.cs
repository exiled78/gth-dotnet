﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gth.Salesforce.SObjects
{
    public class Item : AbstractSObject
    {
        public string Baseline_Missing_Fields__c { get; set; }
        public string DisplayUrl { get; set; }
        public string ExternalDataSource { get; set; }
        public string ExternalDataSourceId { get; set; }
        public string ExternalId { get; set; }
        public string Family { get; set; }
        public string ProductCode { get; set; }
        public string Product_Detail__c { get; set; }
        public string Product_Detail__r { get; set; }
        public string QuantityUnitOfMeasure { get; set; }
        public string StockKeepingUnit { get; set; }
    }
}
