﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gth.Salesforce.SObjects
{
    public class Product : AbstractSObject
    {
        public string Prod_Assigned__c { get; set; }
        public string Prod_Available__c { get; set; }
        public string Prod_Awaiting_Return__c { get; set; }
        public string Prod_Family__c { get; set; }
        public string Prod_In_Fleet__c { get; set; }
        public string Prod_Manufacturer__c { get; set; }
        public string Prod_On_Hire__c { get; set; }
        public string Prod_Product_Family__c { get; set; }
        public string Prod_Products_in_Lieu__c { get; set; }
        public string Prod_Series__c { get; set; }
        public string Prod_Tagged_Out__c { get; set; }

    }
}


