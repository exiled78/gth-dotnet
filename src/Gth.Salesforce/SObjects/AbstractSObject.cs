﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gth.Salesforce.SObjects
{
    public abstract class AbstractSObject
    {
        public string CreatedBy { get; set; }
        public string CreatedById { get; set; }
        public string CreatedDate { get; set; }
        public string Description { get; set; }
        public string Id { get; set; }
        public string IsActive { get; set; }
        public string IsArchived { get; set; }
        public string IsDeleted { get; set; }
        public string LastModifiedBy { get; set; }
        public string LastModifiedById { get; set; }
        public string LastModifiedDate { get; set; }
        public string LastReferencedDate { get; set; }
        public string LastViewedDate { get; set; }
        public string Name { get; set; }
        public string SystemModstamp { get; set; }
    }
}
