﻿using CsvHelper;
using Salesforce.Common;
using Salesforce.Force;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Gth.Salesforce.SObjects;
using static Gth.Salesforce.Constants;


namespace Gth.Salesforce
{
    public class Exporter
    {

        private static string _consumerKey = ConfigurationManager.AppSettings["ConsumerKey"];
        private static string _consumerSecret = ConfigurationManager.AppSettings["ConsumerSecret"];
        private static string _username = ConfigurationManager.AppSettings["Username"];
        private static string _password = ConfigurationManager.AppSettings["Password"];
        private static string _token = ConfigurationManager.AppSettings["Token"];
        private static string _organizationId = ConfigurationManager.AppSettings["OrganizationId"];
        private static string _endpoint = ConfigurationManager.AppSettings["Endpoint"];

        private AuthenticationClient _auth;
        private ForceClient _client;

        public Exporter()
        {
            Init();
        }

        private void Init()
        {
            if (string.IsNullOrEmpty(_consumerKey) && string.IsNullOrEmpty(_consumerSecret) && string.IsNullOrEmpty(_username) && string.IsNullOrEmpty(_password) && string.IsNullOrEmpty(_organizationId))
            {
                _consumerKey = Environment.GetEnvironmentVariable("ConsumerKey");
                _consumerSecret = Environment.GetEnvironmentVariable("ConsumerSecret");
                _username = Environment.GetEnvironmentVariable("Username");
                _password = Environment.GetEnvironmentVariable("Password");
                _organizationId = Environment.GetEnvironmentVariable("OrganizationId");
            }

            // Use TLS 1.2 (instead of defaulting to 1.0)
            const int SecurityProtocolTypeTls11 = 768;
            const int SecurityProtocolTypeTls12 = 3072;
            ServicePointManager.SecurityProtocol |= (SecurityProtocolType)(SecurityProtocolTypeTls12 | SecurityProtocolTypeTls11);

            _auth = new AuthenticationClient();
            _auth.UsernamePasswordAsync(_consumerKey, _consumerSecret, _username, _password+_token,_endpoint).Wait();
            _client = new ForceClient(_auth.InstanceUrl, _auth.AccessToken, _auth.ApiVersion);
        }

        public void DoExport()
        {
            //var resp = this.CreateAccount(_client);
            //var accounts = ParseCsv<Account>("c:/temp/test.csv");
            //Console.WriteLine(accounts.ToString());
            var products = ParseCsv<Product>("C:/Dev/GTH/exports/products.csv");
            var items = ParseCsv<Item>("c:/Dev/GTH/exports/items.csv");
            Console.WriteLine(products.ToString());
        }

        public List<T> ParseCsv<T>(string filePath)
        {
            var stream = new MemoryStream(System.IO.File.ReadAllBytes(filePath));
            var reader = new StreamReader(stream);
            var csvReader = new CsvReader(reader);
            csvReader.Configuration.AutoMap<T>();
            csvReader.Configuration.HasHeaderRecord = true;
            csvReader.Configuration.HeaderValidated = null;
            csvReader.Configuration.MissingFieldFound = null;

            // https://github.com/JoshClose/CsvHelper/issues/1275
            csvReader.Configuration.PrepareHeaderForMatch = (header, index) => header.ToLower();

            var list = new List<T>();
            while (csvReader.Read())
            {
                
                var record = csvReader.GetRecord<T>();
                list.Add(record);
            }

            return list;
        }



        public String CreateAccount(ForceClient client)
        {
            var account = new Account() { Name = "XYZ Account4", Description = "New Account Description4",  AC_Abn__c = "02222267890"};
            //var id = client.CreateAsync(Objects.ACCOUNT, account).Result;
            var lst = client.QueryAsync<Account>("SELECT id, Name, AC_Abn__c FROM Account WHERE AC_Abn__c != '' LIMIT 10").Result;
            return null;
        }
    }

    public class Account
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        // ReSharper disable once InconsistentNaming
        public string AC_Abn__c { get; set; }
    }
}
