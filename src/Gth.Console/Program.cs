﻿using Gth.Salesforce;
using System;

using System.Threading.Tasks;

namespace Gth.Console
{
    class Program
    {
        static void Main(string[] args)
        {

            var exporter = new Exporter();
            exporter.DoExport();

        }
    }
}
