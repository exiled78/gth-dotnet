﻿using Abp.AspNetCore;
using Abp.AspNetCore.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Gth.Configuration;
using Gth.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Gth.Web.Startup
{
    [DependsOn(
        typeof(GthApplicationModule), 
        typeof(GthEntityFrameworkCoreModule), 
        typeof(AbpAspNetCoreModule))]
    public class GthWebModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public GthWebModule(IHostingEnvironment env)
        {
            _appConfiguration = AppConfigurations.Get(env.ContentRootPath, env.EnvironmentName);
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(GthConsts.ConnectionStringName);

            Configuration.Navigation.Providers.Add<GthNavigationProvider>();

            Configuration.Modules.AbpAspNetCore()
                .CreateControllersForAppServices(
                    typeof(GthApplicationModule).GetAssembly()
                );
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(GthWebModule).GetAssembly());
        }
    }
}