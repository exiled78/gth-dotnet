using Microsoft.AspNetCore.Mvc;

namespace Gth.Web.Controllers
{
    public class HomeController : GthControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}