using Abp.AspNetCore.Mvc.Controllers;

namespace Gth.Web.Controllers
{
    public abstract class GthControllerBase: AbpController
    {
        protected GthControllerBase()
        {
            LocalizationSourceName = GthConsts.LocalizationSourceName;
        }
    }
}