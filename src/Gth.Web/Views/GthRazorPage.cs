﻿using Abp.AspNetCore.Mvc.Views;

namespace Gth.Web.Views
{
    public abstract class GthRazorPage<TModel> : AbpRazorPage<TModel>
    {
        protected GthRazorPage()
        {
            LocalizationSourceName = GthConsts.LocalizationSourceName;
        }
    }
}
