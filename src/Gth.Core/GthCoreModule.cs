﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Gth.Localization;

namespace Gth
{
    public class GthCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Auditing.IsEnabledForAnonymousUsers = true;

            GthLocalizationConfigurer.Configure(Configuration.Localization);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(GthCoreModule).GetAssembly());
        }
    }
}