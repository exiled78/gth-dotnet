﻿using Abp.Application.Services;

namespace Gth
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class GthAppServiceBase : ApplicationService
    {
        protected GthAppServiceBase()
        {
            LocalizationSourceName = GthConsts.LocalizationSourceName;
        }
    }
}