﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Gth
{
    [DependsOn(
        typeof(GthCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class GthApplicationModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(GthApplicationModule).GetAssembly());
        }
    }
}